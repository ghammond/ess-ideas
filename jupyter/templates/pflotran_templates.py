# Templates for PFLOTRAN input deck cards
import sys
from string import Template

def grid(nx=1,ny=1,nz=1,lenx=1.,leny=1.,lenz=1.):
    template = '''
GRID
  TYPE STRUCTURED
  NXYZ {} {} {}
  BOUNDS
    0. 0. 0.
    {} {} {}
  /
END'''.format(nx,ny,nz,lenx,leny,lenz)
    return template

grid_defaults = {'nx':1,'ny':1,'nz':1,'lenx':1.,'leny':1.,'lenz':1.}
grid_template = Template('''
GRID
  TYPE STRUCTURED
  NXYZ $nx $ny $nz
  BOUNDS
    0. 0. 0.
    $lenx $leny $lenz
  /
END''')


def main():
    print('Default:')
    print(grid())
    print(grid_template.substitute(grid_defaults))
    print('Non-Default:')
    print(grid(nx=100,lenx=100.))
    print(grid_template.substitute(grid_defaults,nx=100,lenx=100.))
    return 0


if __name__ == "__main__":
    try:
        suite_status = main()
        print("success")
        sys.exit(suite_status)
    except Exception as error:
        print(str(error))
#        if cmdl_options.backtrace:
#            traceback.print_exc()
#        traceback.print_exc()
        print("failure")
        sys.exit(1)

print('done')